#+TITLE: My Personal Emacs Configuration
#+Author: Shafiqur Rahman

* OS and OS Dependency Packages
** OS
   *Linux Mint 20*
** Python Dependency 
   - ~python3-autopep8~
   - ~virtualenvwrapper~
     - make a virtual environment ~problem_solving~
     - ~pip install jedi rope autopep8 yapf flake8 black~

* ORG
** Nice bullets for org mode
   #+BEGIN_SRC emacs-lisp
     (use-package org-bullets
       :ensure t
       :config
       (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
   #+END_SRC
** Spell checker and auto fill
   #+BEGIN_SRC emacs-lisp
     (add-hook 'org-mode-hook 'turn-on-flyspell)
     (add-hook 'org-mode-hook 'turn-on-auto-fill)
     (add-hook 'org-mode-hook 'electric-pair-mode)
   #+END_SRC
** HTML syntax highlights
   #+BEGIN_SRC emacs-lisp
     (use-package htmlize
       :ensure t)
   #+END_SRC
** Easy-to-add emacs-lisp template
   #+BEGIN_SRC emacs-lisp
     (add-to-list 'org-structure-template-alist
		  '("el" "#+BEGIN_SRC emacs-lisp\n?\n#+END_SRC"))
   #+END_SRC
** Org file path store and agenda
   #+BEGIN_SRC emacs-lisp
     (global-set-key (kbd "C-c l") 'org-store-link)
     (global-set-key (kbd "C-c a") 'org-agenda)
   #+END_SRC
** Org babel Language Execution Support Add
   #+BEGIN_SRC emacs-lisp
     (org-babel-do-load-languages
      'org-babel-load-languages
      '((C . t)
	(python . t)))
   #+END_SRC
** TODO For stop lines are modified during export or tangling.
   #+BEGIN_SRC emacs-lisp
     ;;;(setq org-src-preserve-indentation t)
   #+END_SRC
* Basic UI Customization
** Hide the startup message
   #+BEGIN_SRC emacs-lisp
     (setq inhibit-startup-message t)
   #+END_SRC
** Disabling Menu Bar And Scroll Bar mode 
   #+BEGIN_SRC emacs-lisp
     (tool-bar-mode -1)
     (scroll-bar-mode -1)
   #+END_SRC
** Set UTF-8 encoding
   #+BEGIN_SRC emacs-lisp
     (setq locale-coding-system 'utf-8)
     (set-terminal-coding-system 'utf-8)
     (set-keyboard-coding-system 'utf-8)
     (set-selection-coding-system 'utf-8)
     (prefer-coding-system 'utf-8)
   #+END_SRC
** Highlights Current line
   #+BEGIN_SRC emacs-lisp
     (when window-system (add-hook 'prog-mode-hook 'hl-line-mode))
     (when window-system (add-hook 'org-mode-hook 'hl-line-mode))
   #+END_SRC
** Highlights matching parenthesis.
   #+BEGIN_SRC emacs-lisp
     (show-paren-mode 1)
   #+END_SRC
* Shortcuts Customization
** Set 'yes-or-no' to 'y-o-n'
   #+BEGIN_SRC emacs-lisp
     (defalias 'yes-or-no-p 'y-or-n-p)
   #+END_SRC
** Quickly edit ~/.emacs.d/README.org
   #+BEGIN_SRC emacs-lisp
     (defun config-visit ()
       (interactive)
       (find-file "~/.emacs.d/README.org"))
     (global-set-key (kbd "C-c e") 'config-visit)
   #+END_SRC
** C-C r will reload this file.
   #+BEGIN_SRC emacs-lisp
     (defun config-reload ()
       "Reloads ~/.emacs.d/README.org at runtime"
       (interactive)
       (org-babel-load-file (expand-file-name "~/.emacs.d/README.org")))
     (global-set-key (kbd "C-c r") 'config-reload)
   #+END_SRC
** Default Browser
   #+BEGIN_SRC emacs-lisp
     (setenv "BROWSER" "firefox")
   #+END_SRC
** Follow the split screen
   #+BEGIN_SRC emacs-lisp
     (defun split-and-follow-horizontally ()
       (interactive)
       (split-window-below)
       (balance-windows)
       (other-window 1))
     (global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

     (defun split-and-follow-vertically ()
       (interactive)
       (split-window-right)
       (balance-windows)
       (other-window 1))
     (global-set-key (kbd "C-x 3") 'split-and-follow-vertically)
   #+END_SRC
* Theme
** Using Doom theme for the look.
   #+BEGIN_SRC emacs-lisp
     (use-package doom-themes
       :ensure t
       :config
       ;; Global settings (defaults)
       ;; if nil, config is universally disabled
       (setq doom-themes-enable-bold t)
       (setq doom-themes-enable-italic t)
       (load-theme 'doom-one t)
       ;; Corrects (and improves) org-mode's native fontification.
       (doom-themes-org-config)
       ;; Enable flashing mode-line on errors
       (doom-themes-visual-bell-config)
       )
     (use-package doom-modeline
       :ensure t
       :config
       (setq doom-modeline-bar-width 4)
       (setq doom-modeline-major-mode-color-icon t)
       (setq doom-modeline-minor-modes nil)
       (setq doom-modeline-window-width-limit fill-column)
       ;;   truncate-all => ~/P/F/e/l/comint.el
       (setq doom-modeline-buffer-file-name-style 'truncate-all)
       :hook (after-init . doom-modeline-mode))


     ;; Icons for theme
     ;; To install icons need to run
     ;; M-x all-the-icons-install-fonts
     (use-package all-the-icons)

   #+END_SRC
* Counsel
  For previously cut lines at one place
  #+BEGIN_SRC emacs-lisp
    (use-package counsel
      :ensure t
      :bind
      (("M-y" . counsel-yank-pop)
       :map ivy-minibuffer-map
       ("M-y" . ivy-next-line)))
  #+END_SRC
* Swiper
  #+BEGIN_SRC emacs-lisp
    (use-package swiper
      :ensure t
      :bind
      (("C-s" . swiper-isearch)
       ("C-r" . swiper-isearch)
       ("M-x" . counsel-M-x)
       ("C-x C-f" . counsel-find-file)
       ("C-c g" . counsel-git)
       ("C-c j" . counsel-git-grep))
      :config
      (progn
	(setq ivy-use-virtual-buffers t)
	(setq ivy-display-style 'fancy)
	(define-key read-expression-map (kbd "C-r") 'counsel-expression-history)))
  #+END_SRC
* IVY
** Replacement of ido mode 
   #+BEGIN_SRC emacs-lisp
     (use-package ivy
       :ensure t
       :bind (("C-x b" . ivy-switch-buffer))
       :config
       (ivy-mode 1)
       (setq ivy-use-virtual-buffers t)
       (setq ivy-count-format "%d/%d ")
       (setq ivy-display-style 'fancy))
   #+END_SRC
** Pos-frame 
   This makes the ivy completion buffers popup over the mode line
   instead of in the mini buffer.
   #+BEGIN_SRC emacs-lisp
     (use-package ivy-posframe
       :ensure t
       :init
       (ivy-posframe-mode 1)
       :config
       (setq ivy-posframe-display-functions-alist
	     '((swiper          . nil)
	       (complete-symbol . ivy-posframe-display-at-point)
	       (counsel-M-x     . ivy-posframe-display-at-frame-center)
	       (counsel-find-file . ivy-posframe-display-at-frame-center))))
   #+END_SRC
* Projectile
  #+BEGIN_SRC emacs-lisp
	(use-package projectile
	  :ensure t
	  :bind
	  (("C-x p" . projectile-command-map))
	  :config
	  (projectile-mode 1)
	  (setq projectile-completion-system 'ivy)
	  (with-eval-after-load 'projectile
      (add-to-list 'projectile-project-root-files-bottom-up "pubspec.yaml")
      (add-to-list 'projectile-project-root-files-bottom-up "BUILD"))
    )
  #+END_SRC
* Dash Board
  #+BEGIN_SRC emacs-lisp
    (use-package dashboard
      :ensure t
      :init
      (progn
	(setq dashboard-items '((recents . 5) (projects . 5)))
	(setq dashboard-set-file-icons t)
	(setq dashboard-set-heading-icons t))
      :config
      (dashboard-setup-startup-hook))
  #+END_SRC
* Which key
  #+BEGIN_SRC emacs-lisp
    (use-package which-key
      :ensure t
      :config
      (which-key-mode))
  #+END_SRC
* Try Mode
  #+BEGIN_SRC emacs-lisp
    (use-package try
      :ensure t)
  #+END_SRC
* Ace Window
  #+BEGIN_SRC emacs-lisp
    (use-package ace-window
      :ensure t
      :init
      (progn
	(global-set-key [remap other-window] 'ace-window)
	(custom-set-faces
	 '(aw-leading-char-face
	   ((t (:inherit ace-jump-face-foreground :height 3.0)))))))
  #+END_SRC
* Hungry delete
  #+BEGIN_SRC emacs-lisp
    (use-package hungry-delete
      :ensure t
      :config
      (global-hungry-delete-mode))
  #+END_SRC
* Line number mode
  #+BEGIN_SRC emacs-lisp
    (use-package linum-relative
      :ensure t	  
      :config
      (setq linum-relative-current-symbol "1")
      :hook
      (prog-mode . linum-relative-mode)
      (org-mode . linum-relative-mode))
  #+END_SRC
* Multiple Cursor
  #+BEGIN_SRC emacs-lisp
    (use-package multiple-cursors
      :ensure t
      :bind
      (("C->" . mc/mark-next-like-this))
      (("C-<" . mc/mark-previous-like-this))
      (("C-c mc" . mc/mark-all-like-this)))
  #+END_SRC
* Expand Region
  #+BEGIN_SRC emacs-lisp
    (use-package expand-region
      :ensure t
      :config 
      (global-set-key (kbd "C-=") 'er/expand-region))
  #+END_SRC
* Electric add and remove pairs of braces
  #+BEGIN_SRC emacs-lisp
    (setq electric-pair-pairs
	  '((?\{ . ?\})
	    (?\( . ?\))
	    (?\[ . ?\])
	    (?\" . ?\")))
    (add-hook 'c++-mode-hook 'electric-pair-mode)
    (add-hook 'c-mode-hook 'electric-pair-mode)
    (add-hook 'elpy-mode-hook 'electric-pair-mode)
    (add-hook 'php-mode-hook 'electric-pair-mode)
  #+END_SRC
* Beacon Highlights where the cursor goes
  #+BEGIN_SRC emacs-lisp
    (use-package beacon
      :ensure t
      :config
      (beacon-mode 1))
  #+END_SRC
* Yasnippet
  It allows you to type an abbreviation and automatically expand it
  into function templates
  #+BEGIN_SRC emacs-lisp
    (use-package yasnippet
      :ensure t
      :config
      (yas-reload-all)
      (yas-global-mode 1))
    (use-package yasnippet-snippets
      :ensure t)
  #+END_SRC
* Magit
  #+BEGIN_SRC emacs-lisp
    (use-package magit
      :ensure t
      :config
      (setq git-commit-summary-max-length 50)
      :bind
      ("C-x g" . magit-status))

    (setq magit-status-margin
	  '(t "%Y-%m-%d %H:%M " magit-log-margin-width t 18))
  #+END_SRC
* Git Gutter
  #+BEGIN_SRC emacs-lisp
    (use-package git-gutter
      :ensure t
      :diminish (git-gutter-mode)
      :init
      (global-git-gutter-mode t)
      :config
      (custom-set-variables
       '(git-gutter:modified-sign "==") 
       '(git-gutter:added-sign "++")    
       '(git-gutter:deleted-sign "--")))
  #+END_SRC
* Git Time Machine
  #+BEGIN_SRC emacs-lisp
    (use-package git-timemachine
      :ensure t)
  #+END_SRC
* Pretty symbols
  #+BEGIN_SRC emacs-lisp
    (when window-system
      (use-package pretty-mode
	:ensure t
	:config
	(global-pretty-mode t)))
  #+END_SRC
* Python Mode
** Elpy
   #+BEGIN_SRC emacs-lisp
     (use-package elpy
       :ensure t
       :defer t
       :init
       (advice-add 'python-mode :before 'elpy-enable)
       :config
       (when (load "flycheck" nil t)
	 (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))))
   #+END_SRC
** ~pyvenv~ Setup Default Python Virtual Environment
   #+BEGIN_SRC emacs-lisp
     (use-package pyvenv
       :ensure t
       :config
       (pyvenv-workon "problem_solving"))
   #+END_SRC
** blacken 
   #+BEGIN_SRC emacs-lisp
     (use-package blacken
       :ensure t
       :hook (elpy-mode . blacken-mode))
   #+END_SRC
** ~Iedit~ mode for elpy
   #+BEGIN_SRC emacs-lisp
      (use-package iedit
	:ensure t)
   #+END_SRC
** Auto Pep8 
   #+BEGIN_SRC emacs-lisp
     ;; Need the package python3-autopep8
     (use-package py-autopep8
       :ensure t
       :hook
       (elpy-mode . py-autopep8-enable-on-save))
   #+END_SRC
** Python Notebook
   #+BEGIN_SRC emacs-lisp
     (use-package ein
       :ensure t)
   #+END_SRC
* Company Mode
  #+BEGIN_SRC emacs-lisp
    (use-package company
      :ensure t
      :config
      (setq company-idle-delay 0)
      (setq company-minimum-prefix-length 2)
      :hook
      (after-init . global-company-mode))

    (with-eval-after-load 'company
      (define-key company-active-map (kbd "SPC") #'company-abort))
    ;; Company Mode jedi for Python
    (use-package company-jedi
      :ensure t
      :hook
      (elpy-mode . jedi:setup))
    ;; Company Mode for REStclient
    (use-package company-restclient
      :ensure t
      :config
      (add-to-list 'company-backends 'company-restclient))
    ;; Company language package for PHP
    (use-package company-php
      :defer
      :after company)
  #+END_SRC
* Web Mode
  #+BEGIN_SRC emacs-lisp
    (use-package web-mode
      :ensure t
      :config
      (progn
	(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.blade\\.php\\'" . web-mode))
	(add-to-list 'auto-mode-alist '("\\.vue?\\'" . web-mode))
	(setq web-mode-engines-alist '(("php" . "\\.php\\'")))
	(setq web-mode-engines-alist '(("blade" . "\\.blade\\.'")))
	(setq web-mode-engines-alist '(("django" . "\\.html\\'")))
	(setq web-mode-markup-indent-offset 2)
	(setq web-mode-css-indent-offset 2)
	(setq web-mode-code-indent-offset 2)
	;(setq web-mode-enable-auto-pairing t)
	(setq web-mode-enable-current-column-highlight t)
	))
  #+END_SRC
* Impatient Mode
    #+BEGIN_SRC emacs-lisp
      ;;; package --- Summary
      ;;; Commentary:
      ;;; To start M-x httpd-start and M-x impatient-mode
      ;;; Code:
      (use-package impatient-mode
	:ensure t)
    #+END_SRC
* Emmet Mode
  #+BEGIN_SRC emacs-lisp
    (use-package cl
      :ensure t)
    (use-package emmet-mode
      :ensure t
      :hook
      (sgml-mode . emmet-mode)
      (web-mode . emmet-mode)
      (css-mode . emmet-mode))
  #+END_SRC
* PHP Mode
** For Pure PHP file  
   #+BEGIN_SRC emacs-lisp
     (use-package php-mode
       :ensure t)
   #+END_SRC
* ~Flycheck~
** For checking error on the fly
      #+BEGIN_SRC emacs-lisp
	(use-package flycheck
	  :ensure t
	  :init (global-flycheck-mode))
      #+END_SRC
* REST client
  #+BEGIN_SRC emacs-lisp
    (use-package restclient
      :ensure t)
  #+END_SRC
* Lsp
  #+BEGIN_SRC emacs-lisp
    ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
    (setq lsp-keymap-prefix "C-c z")

    (use-package lsp-mode
	:hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
		(dart-mode . lsp)
		;; if you want which-key integration
		(lsp-mode . lsp-enable-which-key-integration))
	:commands lsp
	:init
	(setq lsp-dart-sdk-dir "~/workspace/dart/flutter/flutter/bin/cache/dart-sdk")
	(setq lsp-dart-flutter-sdk-dir "~/workspace/dart/flutter/flutter"))
    (use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
    (use-package lsp-treemacs :commands lsp-treemacs-errors-list)
    ; optionally if you want to use debugger
    (use-package dap-mode :ensure t)
    ;;(use-package dap-dart :ensure t) 
    (use-package lsp-dart
      :ensure t
      :hook (dart-mode . lsp))
    (use-package flutter
      :ensure t
      :after dart-mode
      :bind (:map dart-mode-map
		  ("C-M-x" . #'flutter-run-or-hot-reload))
      :custom
      (flutter-sdk-path "~/workspace/dart/flutter/flutter/"))
  #+END_SRC
* ~Treemacs~
  #+BEGIN_SRC emacs-lisp
    (use-package treemacs
      :ensure t
      :bind
      (:map global-map
	    ([f8] . treemacs)
	    ("C-<f8>" . treemacs-select-window))
      :config
      (setq treemacs-is-never-other-window t))
  #+END_SRC
