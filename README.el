(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(add-hook 'org-mode-hook 'turn-on-flyspell)
(add-hook 'org-mode-hook 'turn-on-auto-fill)
(add-hook 'org-mode-hook 'electric-pair-mode)

(use-package htmlize
  :ensure t)

(add-to-list 'org-structure-template-alist
	     '("el" "#+BEGIN_SRC emacs-lisp\n?\n#+END_SRC"))

(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((C . t)
   (python . t)))

;;;(setq org-src-preserve-indentation t)

(setq inhibit-startup-message t)

(tool-bar-mode -1)
(scroll-bar-mode -1)

(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(when window-system (add-hook 'prog-mode-hook 'hl-line-mode))
(when window-system (add-hook 'org-mode-hook 'hl-line-mode))

(show-paren-mode 1)

(defalias 'yes-or-no-p 'y-or-n-p)

(defun config-visit ()
  (interactive)
  (find-file "~/.emacs.d/README.org"))
(global-set-key (kbd "C-c e") 'config-visit)

(defun config-reload ()
  "Reloads ~/.emacs.d/README.org at runtime"
  (interactive)
  (org-babel-load-file (expand-file-name "~/.emacs.d/README.org")))
(global-set-key (kbd "C-c r") 'config-reload)

(setenv "BROWSER" "firefox")

(defun split-and-follow-horizontally ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

(defun split-and-follow-vertically ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vertically)

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  ;; if nil, config is universally disabled
  (setq doom-themes-enable-bold t)
  (setq doom-themes-enable-italic t)
  (load-theme 'doom-one t)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config)
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  )
(use-package doom-modeline
  :ensure t
  :config
  (setq doom-modeline-bar-width 4)
  (setq doom-modeline-major-mode-color-icon t)
  (setq doom-modeline-minor-modes nil)
  (setq doom-modeline-window-width-limit fill-column)
  ;;   truncate-all => ~/P/F/e/l/comint.el
  (setq doom-modeline-buffer-file-name-style 'truncate-all)
  :hook (after-init . doom-modeline-mode))


;; Icons for theme
;; To install icons need to run
;; M-x all-the-icons-install-fonts
(use-package all-the-icons)

(use-package counsel
  :ensure t
  :bind
  (("M-y" . counsel-yank-pop)
   :map ivy-minibuffer-map
   ("M-y" . ivy-next-line)))

(use-package swiper
  :ensure t
  :bind
  (("C-s" . swiper-isearch)
   ("C-r" . swiper-isearch)
   ("M-x" . counsel-M-x)
   ("C-x C-f" . counsel-find-file)
   ("C-c g" . counsel-git)
   ("C-c j" . counsel-git-grep))
  :config
  (progn
    (setq ivy-use-virtual-buffers t)
    (setq ivy-display-style 'fancy)
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)))

(use-package ivy
  :ensure t
  :bind (("C-x b" . ivy-switch-buffer))
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "%d/%d ")
  (setq ivy-display-style 'fancy))

(use-package ivy-posframe
  :ensure t
  :init
  (ivy-posframe-mode 1)
  :config
  (setq ivy-posframe-display-functions-alist
	'((swiper          . nil)
	  (complete-symbol . ivy-posframe-display-at-point)
	  (counsel-M-x     . ivy-posframe-display-at-frame-center)
	  (counsel-find-file . ivy-posframe-display-at-frame-center))))

(use-package projectile
      :ensure t
      :bind
      (("C-x p" . projectile-command-map))
      :config
      (projectile-mode 1)
      (setq projectile-completion-system 'ivy)
      (with-eval-after-load 'projectile
  (add-to-list 'projectile-project-root-files-bottom-up "pubspec.yaml")
  (add-to-list 'projectile-project-root-files-bottom-up "BUILD"))
)

(use-package dashboard
  :ensure t
  :init
  (progn
    (setq dashboard-items '((recents . 5) (projects . 5)))
    (setq dashboard-set-file-icons t)
    (setq dashboard-set-heading-icons t))
  :config
  (dashboard-setup-startup-hook))

(use-package which-key
  :ensure t
  :config
  (which-key-mode))

(use-package try
  :ensure t)

(use-package ace-window
  :ensure t
  :init
  (progn
    (global-set-key [remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))))

(use-package hungry-delete
  :ensure t
  :config
  (global-hungry-delete-mode))

(use-package linum-relative
  :ensure t	  
  :config
  (setq linum-relative-current-symbol "1")
  :hook
  (prog-mode . linum-relative-mode)
  (org-mode . linum-relative-mode))

(use-package multiple-cursors
  :ensure t
  :bind
  (("C->" . mc/mark-next-like-this))
  (("C-<" . mc/mark-previous-like-this))
  (("C-c mc" . mc/mark-all-like-this)))

(use-package expand-region
  :ensure t
  :config 
  (global-set-key (kbd "C-=") 'er/expand-region))

(setq electric-pair-pairs
      '((?\{ . ?\})
	(?\( . ?\))
	(?\[ . ?\])
	(?\" . ?\")))
(add-hook 'c++-mode-hook 'electric-pair-mode)
(add-hook 'c-mode-hook 'electric-pair-mode)
(add-hook 'elpy-mode-hook 'electric-pair-mode)
(add-hook 'php-mode-hook 'electric-pair-mode)

(use-package beacon
  :ensure t
  :config
  (beacon-mode 1))

(use-package yasnippet
  :ensure t
  :config
  (yas-reload-all)
  (yas-global-mode 1))
(use-package yasnippet-snippets
  :ensure t)

(use-package magit
  :ensure t
  :config
  (setq git-commit-summary-max-length 50)
  :bind
  ("C-x g" . magit-status))

(setq magit-status-margin
      '(t "%Y-%m-%d %H:%M " magit-log-margin-width t 18))

(use-package git-gutter
  :ensure t
  :diminish (git-gutter-mode)
  :init
  (global-git-gutter-mode t)
  :config
  (custom-set-variables
   '(git-gutter:modified-sign "==") 
   '(git-gutter:added-sign "++")    
   '(git-gutter:deleted-sign "--")))

(use-package git-timemachine
  :ensure t)

(when window-system
  (use-package pretty-mode
    :ensure t
    :config
    (global-pretty-mode t)))

(use-package elpy
  :ensure t
  :defer t
  :init
  (advice-add 'python-mode :before 'elpy-enable)
  :config
  (when (load "flycheck" nil t)
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))))

(use-package pyvenv
  :ensure t
  :config
  (pyvenv-workon "problem_solving"))

(use-package blacken
  :ensure t
  :hook (elpy-mode . blacken-mode))

(use-package iedit
  :ensure t)

;; Need the package python3-autopep8
(use-package py-autopep8
  :ensure t
  :hook
  (elpy-mode . py-autopep8-enable-on-save))

(use-package ein
  :ensure t)

(use-package company
  :ensure t
  :config
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 2)
  :hook
  (after-init . global-company-mode))

(with-eval-after-load 'company
  (define-key company-active-map (kbd "SPC") #'company-abort))
;; Company Mode jedi for Python
(use-package company-jedi
  :ensure t
  :hook
  (elpy-mode . jedi:setup))
;; Company Mode for REStclient
(use-package company-restclient
  :ensure t
  :config
  (add-to-list 'company-backends 'company-restclient))
;; Company language package for PHP
(use-package company-php
  :defer
  :after company)

(use-package web-mode
  :ensure t
  :config
  (progn
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.blade\\.php\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.vue?\\'" . web-mode))
    (setq web-mode-engines-alist '(("php" . "\\.php\\'")))
    (setq web-mode-engines-alist '(("blade" . "\\.blade\\.'")))
    (setq web-mode-engines-alist '(("django" . "\\.html\\'")))
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-css-indent-offset 2)
    (setq web-mode-code-indent-offset 2)
    ;(setq web-mode-enable-auto-pairing t)
    (setq web-mode-enable-current-column-highlight t)
    ))

;;; package --- Summary
;;; Commentary:
;;; To start M-x httpd-start and M-x impatient-mode
;;; Code:
(use-package impatient-mode
  :ensure t)

(use-package cl
  :ensure t)
(use-package emmet-mode
  :ensure t
  :hook
  (sgml-mode . emmet-mode)
  (web-mode . emmet-mode)
  (css-mode . emmet-mode))

(use-package php-mode
  :ensure t)

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package restclient
  :ensure t)

;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
(setq lsp-keymap-prefix "C-c z")

(use-package lsp-mode
    :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
	    (dart-mode . lsp)
	    ;; if you want which-key integration
	    (lsp-mode . lsp-enable-which-key-integration))
    :commands lsp
    :init
    (setq lsp-dart-sdk-dir "~/workspace/dart/flutter/flutter/bin/cache/dart-sdk")
    (setq lsp-dart-flutter-sdk-dir "~/workspace/dart/flutter/flutter"))
(use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)
; optionally if you want to use debugger
(use-package dap-mode :ensure t)
;;(use-package dap-dart :ensure t) 
(use-package lsp-dart
  :ensure t
  :hook (dart-mode . lsp))
(use-package flutter
  :ensure t
  :after dart-mode
  :bind (:map dart-mode-map
	      ("C-M-x" . #'flutter-run-or-hot-reload))
  :custom
  (flutter-sdk-path "~/workspace/dart/flutter/flutter/"))

(use-package treemacs
  :ensure t
  :bind
  (:map global-map
	([f8] . treemacs)
	("C-<f8>" . treemacs-select-window))
  :config
  (setq treemacs-is-never-other-window t))
